from sklearn import datasets
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics


data = datasets.load_iris()
#Для удобства работы можно импортировать Pandas и сохранить информацию в табличном виде
# Создадим DataFrame.
df = pd.DataFrame(data.data, columns=data.feature_names)
# Добавим столбец "target" и заполним его данными.
df['target'] = data.target


#В столбце target указан вид, к которому относится цветок: setosa (0), versicolor (1), virginica (2)

species = []

for i in range(len(df['target'])):
   if df['target'][i] == 0:
       species.append("setosa")
   elif df['target'][i] == 1:
       species.append('versicolor')
   else:
       species.append('virginica')

df['species'] = species



# Удалим из датасета два столбца, которые не нужны нам для обучения.
X = df.drop(['target','species'], axis=1)

# Переведём значения длины и ширины чашелистика в массив NumPy.
X = X.to_numpy()[:, (2,3)]
y = df['target']

# Разделим данные на тестовую и тренировочную выборку.
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.5, random_state=42)


log_reg = LogisticRegression()
log_reg.fit(X_train,y_train) # Обучаем наш алгоритм на тренировочной выборке.

training_prediction = log_reg.predict(X_train)
print(training_prediction)
test_prediction = log_reg.predict(X_test)
print(test_prediction)

print(metrics.classification_report(y_train, training_prediction))

print(metrics.classification_report(y_test, test_prediction))


accuracy = log_reg.score(X_test, y_test)
print(accuracy)

codecs = ["cp1252", "cp437", "utf-16be", "utf-16"]
# Сохраните результаты классификации в файл predict.txt
with open("predict.txt", "w",  encoding="utf-16be") as file:
    for prediction in test_prediction:
        file.write(str(prediction))
        print(str(prediction) + ' ')
